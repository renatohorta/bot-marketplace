import re
import json
import scrapy
import time 
import urllib
from bs4 import BeautifulSoup

from EntityManager import EntityManager

hash_tags = ['carnavalbh'
    , 'blococarnavalbh'
    , 'blococarnavalbh2017'
    , 'carnavalbh2017'
    , 'carnavalizabh'
    , 'carnavalizabh2017'
#    , 'carnavalbhsempreconceito'
#    , 'carnavalbh2017lgbt'
#    , 'carnavalderuabh'
#    , 'carnavalisabh' 
#    , 'carnavalizabetim'
#    , 'carnavalizabetim2017'
#    , 'pulabh'
#    , 'blocoderuabh'
#    , 'havayanasusadas'
#    , 'descentralizabh'
#    , 'descentralizabh2017'
#    , 'precarnavalbh'
#    , 'precarnavalbh2017'
#    , 'praiadaestacaobh'
#    , 'carnavalizabelo'
#    , 'blocosderuabh'
    , 'praiadaestacao'

]

connection_dict = {
'connection_server': 'ec2-54-201-223-109.us-west-2.compute.amazonaws.com',
'connection_login': 'talkabot',
'connection_password': 'talkabot01',
'connection_database': 'karneval'
#'connection_server': 'localhost',
#'connection_login': 'root',
#'connection_password': 'admin',
#'connection_database': 'karneval'
}

QUERY_INSERT_USER_SOCIAL_MEDIA = """INSERT INTO karneval.social_media_user 
(id_social_media, user_name, user_email, user_login, user_profile_url, user_profile_image_url) 
VALUES (2,  %s,  %s,  %s,  %s,  %s) """

QUERY_INSERT_POST = """INSERT INTO karneval.post (id_user, post_date, CODE)
VALUES ((SELECT max(id) FROM karneval.social_media_user WHERE id_social_media = 2 AND user_login = %s) , %s, %s) """

QUERY_INSERT_POST_INFO = """INSERT INTO karneval.post_info (id_post, post_info, main_media_url, main_media_type, main_media_thumb)
VALUES ((SELECT max(id) FROM karneval.post WHERE CODE = %s), %s, %s, %s, %s) """

QUERY_INSERT_POST_HASH_TAG = """INSERT INTO karneval.post_hashtag (id_post, hashtag) VALUES 
((SELECT max(id) FROM karneval.post WHERE CODE = %s), %s) """


def save_item(item):
    main_em = EntityManager(connection_dict)
    main_em.create_connection_cursor()

    query_params_sm = (item['user_name'], None, item['user_login'], 
                    item['user_profile_url'], item['user_profile_image_url'])
    result = main_em.insert_batch(QUERY_INSERT_USER_SOCIAL_MEDIA, query_params_sm)

    query_params_po = (item['user_login'], item['post_date'], item['code'])
    result = main_em.insert_batch(QUERY_INSERT_POST, query_params_po)

    query_params_pi = (item['code'], item['post_info'], item['main_media_url'],
                       item['main_media_type'], item['main_media_thumb'])
    result = main_em.insert_batch(QUERY_INSERT_POST_INFO, query_params_pi)

    for hashtag in item['hashtag_list']:
        query_params_ht = (item['code'], hashtag)
        result = main_em.insert_batch(QUERY_INSERT_POST_HASH_TAG, query_params_ht)

    main_em.connection.commit()
    main_em.cursor.close()

def get_extracted(value, index=0):
    try:
        return value[index]
    except:
        return ""

class InstagramPost(scrapy.Item):
    # User
    user_name = scrapy.Field() #
    user_login = scrapy.Field() #
    user_profile_url = scrapy.Field() #
    user_profile_image_url = scrapy.Field() #
    
    # Post
    post_date = scrapy.Field() #
    code = scrapy.Field() #

    # Post Info
    post_info = scrapy.Field() #
    main_media_url = scrapy.Field() #
    main_media_thumb = scrapy.Field() #
    main_media_type = scrapy.Field()

    # HashTag HashTag
    hashtag_list = scrapy.Field() #


class InstagramPostListener(scrapy.Spider):
    name = 'instagram-lister'
    
    start_urls = ['https://www.instagram.com/explore/tags/%s' % item for item in hash_tags]
    
    name = "Instagram"
    
    tag=''
    
    page_cnt=3 # return count=page_cnt*20
    
    params = {
        'access_token': '',
    }

    def parse(self, response):
        javascript = "".join(response.xpath('//script[contains(text(), "sharedData")]/text()').extract())
        json_data = json.loads("".join(re.findall(r'window._sharedData = (.*);', javascript)))
        
        top_post_list = [item for item in json_data["entry_data"]['TagPage'][0]['tag']['top_posts']['nodes']]
        top_post_list_code = [item['code'] for item in top_post_list]

        media_list = [item for item in json_data["entry_data"]['TagPage'][0]['tag']['media']['nodes']]
        media_list =  [item for item in media_list if item['code'] not in top_post_list_code]

        for post in media_list + top_post_list:
            item = InstagramPost()
            if not post['is_video']:
                item['code'] = post['code']
                #import pdb;pdb.set_trace()
                item['hashtag_list'] = [cap_item.strip() for cap_item in post['caption'].split(' ') if cap_item.startswith('#')]
                item['post_info'] = post['caption']#.decode('iso8859-1')
                item['main_media_url'] = post['display_src']
                item['main_media_thumb'] = post['thumbnail_src']
                item['post_date'] = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(post['date']))
                item['main_media_type'] = 'IMAGE'

                post_page = urllib.urlopen('https://www.instagram.com/p/' + post['code']).read()
                soup = BeautifulSoup(post_page)
                script_tag = soup.find('script', text=re.compile('window\._sharedData'))
                shared_data = script_tag.string.partition('=')[-1].strip(' ;')
                post_user = json.loads(shared_data)
                item['user_name'] = post_user['entry_data']['PostPage'][0]['media']['owner']['full_name']#.decode('iso8859-1')
                item['user_login'] = post_user['entry_data']['PostPage'][0]['media']['owner']['username']
                
                item['user_profile_image_url'] = post_user['entry_data']['PostPage'][0]['media']['owner']['profile_pic_url']
                item['user_profile_url'] = 'https://www.instagram.com/' + item['user_login'] 

                save_item(item)
                
                print 'post included for hashtag ' + response.url + ' code :' + item['code']

                #next_page = json_data['entry_data']['TagPage'][0]['tag']['media']['page_info']['has_next_page']
                #if next_page:
                #    tag_name = json_data['entry_data']['TagPage'][0]['tag']['name']
                #    end_cursor = json_data['entry_data']['TagPage'][0]['tag']['media']['page_info']['end_cursor']
                #    next_page_url = 'https://www.instagram.com/explore/tags/%s/?max_id=%s' % (tag_name, end_cursor)
                #    request = scrapy.Request(url=next_page_url)
                #    yield request




