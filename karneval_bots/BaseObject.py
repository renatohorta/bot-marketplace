# -*- coding: utf-8 -*-
import os
import time

class BaseObject:

    def __init__(self):
        self.file_log = None

    def start_log(self):
        self.register_log('Process Start')

    def close_log(self):
        pass

    def print_msg(self, msg):
        msg = ' [' + time.strftime("%c") + '] - ' + msg
        print msg
        
    def register_log(self, msg):
        msg = ' [' + time.strftime("%c") + '] - ' + msg
        print msg
