# -*- coding: utf-8 -*-

import os
import json
import urllib

from correios import BaseBot
from correios import config

BASE_CEP_URL = 'http://cep.correiocontrol.com.br/<CEP_CORREIO>.json'

faixa_cep = {
    'AC': '69900000;69999999',
    'AL': '57000000;57999999',
    'AM': '69000000;69299999',
    'AM': '69400000;69899999',
    'AP': '68900000;68999999',
    'BA': '40000000;48999999',
    'CE': '60000000;63999999',
    'DF': '70000000;72799999',
    'DF': '73000000;73699999',
    'ES': '29000000;29999999',
    'GO': '72800000;72999999',
    'GO': '73700000;76999999',
    'MA': '65000000;65999999',
    'MG': '30000000;39999999',
    'MS': '79000000;79999999',
    'MT': '78000000;78899999',
    'PA': '66000000;68899999',
    'PB': '58000000;58999999',
    'PE': '50000000;56999999',
    'PI': '64000000;64999999',
    'PR': '80000000;87999999',
    'RJ': '20000000;28999999',
    'RN': '59000000;59999999',
    'RO': '78900000;78999999',
    'RR': '69300000;69399999',
    'RS': '90000000;99999999',
    'SC': '88000000;89999999',
    'SE': '49000000;49999999',
    'SP': '01000000;19999999',
    'TO': '77000000;77999999',
}
class BuscaCEPBot(BaseBot):

    def __init__(self, bot_key):
        BaseBot.__init__(self, bot_key)
        self.json_data = None
        self.set_mtg = None
        self.cards = None
        self.card_data = None
        self.current_card_id = None

    def update_attribuites(self, config_dict):
        BaseBot.update_attribuites(self, config_dict)

    def execute(self):
        self.process_busca_cep()

    def load_json_file(self, json_data):
        if (not 'correiocontrolcep' in json_data):
            self.register_log('JSON File Load Start')
            self.json_data = json.loads(json_data)
            self.register_log('JSON File Load End')

    def get_formated_cep(self, cep_number):
        return '0' * (8 - len(str(cep_number))) + str(cep_number)
        
    def process_busca_cep(self):
        for item in faixa_cep:
            range_init = int(faixa_cep[item].split(';')[0])
            range_end = int(faixa_cep[item].split(';')[1])
            for item in range (range_init, range_end):
                url = BASE_CEP_URL.replace('<CEP_CORREIO>', self.get_formated_cep(item))
                self.register_log('Buscando CEP: ' + url)
                f = urllib.urlopen(url)
                self.load_json_file(f.read())
                if self.json_data:
                    #if (self.json_data.has_key('')
                    for key in self.json_data:
                        print key + ': ' + self.json_data[key]
    