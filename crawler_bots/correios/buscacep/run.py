import sys
sys.path.append('../..')

from correios import config
from BuscaCEPBot import BuscaCEPBot

if __name__ == "__main__":
    cep_bot = BuscaCEPBot()
    cep_bot.run_bot()
    