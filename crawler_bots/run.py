# -*- coding: utf-8 -*-

from tcgcards.basecard.BaseCardBot import BaseCardBot
from correios.buscacep.BuscaCEPBot import BuscaCEPBot

if __name__ == "__main__":
    base_card_bot = BaseCardBot('TCG_CARDS')
    base_card_bot.run_bot()
    #cep_bot = BuscaCEPBot('base_card')
    #cep_bot.run_bot()