# -*- coding: utf-8 -*-

import MySQLdb as mdb

from BaseObject import BaseObject
class EntityManager(BaseObject):
    
    def __init__(self, connection_dict, bot_key):
        BaseObject.__init__(self, bot_key)
        self.connection_server = connection_dict['connection_server']
        self.connection_login = connection_dict['connection_login']
        self.connection_password = connection_dict['connection_password']
        self.connection_database = connection_dict['connection_database']
        self.create_connection_db()
        self.create_connection_cursor()
        self.start_log()
    
    def create_connection_db(self):
        self.connection = mdb.connect(self.connection_server, self.connection_login, self.connection_password, self.connection_database)
        
    def create_connection_cursor(self):
        self.cursor = self.connection.cursor()
        self.cursor.execute('SET autocommit = 0')
        self.connection.commit()

    def insert_batch(self, query, query_params=None, merge_query=True):
        result = None
        if merge_query:
            query = self.create_merge_query(query)
        try:
            query = query.replace('\n', '')
            self.cursor.execute(query + ';', query_params)
            result = self.cursor.lastrowid
        except:
            import pdb;pdb.set_trace()
            self.register_log('BATCH ERROR : ' + query)
            self.connection.rollback()
            return -1
        return result
    
    def insert(self, query, merge_query=True):
        result = None
        self.create_connection_cursor()
        if merge_query:
            query = self.create_merge_query(query)
        try:
            self.cursor.execute(query + ';')
            self.connection.commit()
            result = self.cursor.lastrowid
        except:
            self.register_log('Query : ' + query)
            self.connection.rollback()
            return -1
        self.cursor.close()
        return result

    def query(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()
        
    def create_merge_query(self, query):
        duplicate_items = query.split('VALUES')[0].split('(')[1].strip().replace(')','').replace('`', '').split(',')
        on_duplicate = ' ON DUPLICATE KEY UPDATE '
        for item in duplicate_items:
            if item != 'id':
                on_duplicate += item + '=' + item
                if item != duplicate_items[-1]:
                    on_duplicate += ','
        return query + on_duplicate
