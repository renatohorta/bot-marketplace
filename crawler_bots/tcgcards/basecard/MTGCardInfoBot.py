#http://magiccards.info/query?q=%22Advantageous+Proclamation%22&v=card&s=cname
from bs4 import BeautifulSoup
import urllib

class MTGCardInfoBot(BaseObject):

	BASE_MTG_CARD_INFO = 'http://magiccards.info/'
	BASE_SEARCH_URL = 'http://magiccards.info/query?q=%s e:%s'

    def __init__(self, bot_key):
        BaseObject.__init__(self, bot_key)

	def parse_card_name(self, card_name):
		return '"' + card_name.strip().replace(' ', '+')

	def search_card_info_image_url(self, card_name, card_set):
		url = BASE_SEARCH_URL % (self.parse_card_name(card_name), card_set)
		url_img_cards = [BASE_MTG_CARD_INFO + item.get('href') for item in soup.find_all("a") 
					if item.get('href').split('/')[1] == card_set
						and str(item.get('href').split('/')[-1].replace('.html', '')).isdigit() 
						and str(item.text) != 'Backup Plan']
		result = []
		for item in url_cards:
			result.add(self.crawl_card_info_image_url(item, card_name))
		return results
		
	def crawl_card_info_image_url(self, url, card_name)	
		r = urllib.urlopen(url).read()
		soup = BeautifulSoup(r)

		main_img_list = [item for item in soup.find_all("img") if item.get('alt') == card_name ]
		if main_img_list:
			return main_img_list[0].get('src')
		else:
			self.register_log("Could not load image for card: " + card_name + " set " + card_set)
			return

		
