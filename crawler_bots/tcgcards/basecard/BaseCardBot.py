# -*- coding: utf-8 -*-

import os
import json
import urllib
import hashlib

from tcgcards import BaseBot
from tcgcards import config


INSERT_MAGIC_SET = """INSERT INTO poyta.magic_set (code, name, gatherer_code, release_date, border, set_type, block, old_code, magic_cards_info_code, online_only) 
                      VALUES (%s , %s , %s , %s , %s , %s , %s, %s , %s , %s)"""

INSERT_MAGIC_CARD = """ INSERT INTO poyta.magic_card (card_ucode, layout, name, mana_cost, cmc, card_type, rarity, text, flavor, artist, number, power, toughness, loyalty, multiverseid, image_name, watermark, border, hand, life, original_text, original_type, timeshifted, reserved, release_date, starter, mci_number, source, id_magic_set)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, (SELECT a.ID FROM poyta.magic_set a WHERE a.code = %s)) """

INSERT_MAGIC_BOOSTER = """ INSERT INTO poyta.magic_set_booster (booster_name, position_index, id_magic_set) VALUES (%s, %s, (SELECT a.ID FROM poyta.magic_set a WHERE a.code = %s)) """

BASE_SELECT_CARD = """ SELECT c.ID FROM poyta.magic_card c WHERE c.card_ucode = %s """

BASE_IMAGE_URL = """http://magiccards.info/scans/<LANGUAGE_CODE>/<set_code>/<card_name>.jpg"""
#http://magiccards.info/scans/tw/kld/191.jpg
LANGUAGE_DICT = {
'Chinese Simplified': 'cn',
'Chinese Traditional': 'tw',
'French': 'fr',
'German': 'de',
'Italian': 'it',
'Japanese': 'jp',
'Korean': 'ko',
'Portuguese (Brazil)': 'pt',
'Russian': 'ru',
'Spanish': 'es',
'English': 'en'
}

class BaseCardBot(BaseBot):

    def __init__(self, bot_key):
        BaseBot.__init__(self, bot_key)
        self.json_data = None
        self.set_mtg = None
        self.cards = None
        self.card_data = None
        self.current_card_id = None
        self.labels_dict = {}

    def update_attribuites(self, config_dict):
        BaseBot.update_attribuites(self, config_dict)

    def get_parsed_string(self, value):
        result = None
        if value:
            result = str(value.encode('utf-8'))
        return result
    
    def get_parsed_mtg(self, key):
        result = None
        if self.set_mtg != None and self.set_mtg.has_key(key):
            if isinstance(self.set_mtg[key], bool):
                return self.set_mtg[key]
            result = str(self.set_mtg[key].encode('utf-8'))
        return result

    def get_parsed_card(self, key, sql_mode=True):
        result = None
        if self.card_data != None and self.card_data.has_key(key):
            #if self.card_data.has_key('reserved'):
            #    import pdb;pdb.set_trace()
            if isinstance(self.card_data[key], bool):
                return self.card_data[key]
            return str(self.card_data[key]).encode('utf-8').replace('\n', '<br>')
        return result

    def execute(self):
        self.load_json_file()
        self.process_set()
        self.process_boster()
        self.process_card()
        self.process_meta_card_info()
        
    def load_json_file(self):
        self.register_log('JSON File Load Start')
        path = os.path.join(self.config_bot['entry_file_default_path'], self.config_bot['json_file'])
        with open(path) as json_data:
            self.json_data = json.load(json_data, "utf8")
        self.register_log('JSON File Load End')

    def process_set(self):
        for key in self.json_data:
            self.set_mtg = self.json_data[key]
            query_params =  (self.get_parsed_mtg('code'), self.get_parsed_mtg('name'), self.get_parsed_mtg('gathererCode'), self.get_parsed_mtg('releaseDate'), self.get_parsed_mtg('border'), self.get_parsed_mtg('type'), self.get_parsed_mtg('block'), self.get_parsed_mtg('oldCode'), self.get_parsed_mtg('magicCardsInfoCode'), self.get_parsed_mtg('onlineOnly'))
            self.main_em.create_connection_cursor()
            result = self.main_em.insert_batch(INSERT_MAGIC_SET, query_params)
            if result >= 0:
                self.register_log('SET included: ' + self.get_parsed_mtg('code'))
            else:
                self.register_log('ERROR SET included: ' + query)
                return
        self.main_em.connection.commit()
        self.main_em.cursor.close()
        
    def process_boster(self):
        # Criar campo co_seq na tabela para fazer parte da chave e dar somente update;
        self.register_log('Including Booster - Start')
        self.main_em.create_connection_cursor()
        for key in self.json_data:
            self.set_mtg = self.json_data[key]
            set_code = self.get_parsed_mtg('code')
            if self.set_mtg.has_key('booster'):
                set_booster = self.set_mtg['booster']
                count = 0
                for item in set_booster:
                    if not hasattr(item, '__iter__'):
                        query_params = (self.get_parsed_string(item), count, set_code)
                    else:
                        #TODO: create 2 fields for booster label
                        query_params = (self.get_parsed_string(str(item[0]) + ' - ' + str(item[1])), count, set_code)
                    result = self.main_em.insert_batch(INSERT_MAGIC_BOOSTER, query_params)
                    if result < 0:
                        self.register_log('ERRO BOOSTER SET: ' + set_code)
                        return
                    count += 1
                self.register_log('SET BOOSTER included: ' + set_code)
        self.main_em.connection.commit()
        self.main_em.cursor.close()

    def get_card_id(self):
        cursor = self.config_em.cursor
        base_query = ' SELECT c.ID FROM poyta.magic_card c WHERE c.card_ucode = \'{0}\' '.format(self.get_parsed_card('id').strip())
        cursor.execute(base_query)
        result = cursor.fetchall()
        if result and len(result) > 0:
            return result[0][0]
        else:
            import pdb;pdb.set_trace()
    
    def process_card(self):
        self.register_log('Including Cards - Start')
        count = 0
        self.main_em.create_connection_cursor()
        for key in self.json_data:
            self.set_mtg = self.json_data[key]
            set_code = self.set_mtg['code']
            self.cards = self.json_data[key]['cards']
            for card_data in self.cards:
                self.card_data = card_data
                self.process_json_file();
                query_params = (self.get_parsed_card('id'), self.get_parsed_card('layout'), self.get_parsed_card('name'), self.get_parsed_card('manaCost'), self.get_parsed_card('cmc'),
                                self.get_parsed_card('type'), self.get_parsed_card('rarity'), self.get_parsed_card('text'), self.get_parsed_card('flavor'),
                                self.get_parsed_card('artist'), self.get_parsed_card('number'), self.get_parsed_card('power'), self.get_parsed_card('toughness'),
                                self.get_parsed_card('loyalty'), self.get_parsed_card('multiverseid'), self.get_parsed_card('imageName'), self.get_parsed_card('watermark'),
                                self.get_parsed_card('border'), self.get_parsed_card('hand'), self.get_parsed_card('life'), self.get_parsed_card('originalText'),
                                self.get_parsed_card('originalType'), self.get_parsed_card('timeshifted'), self.get_parsed_card('reserved'),self.get_parsed_card('releaseDate'), 
                                self.get_parsed_card('starter'), self.get_parsed_card('mci_number'), self.get_parsed_card('source'), set_code)

                result = self.main_em.insert_batch(INSERT_MAGIC_CARD, query_params)
                
                if result >= 0:
                    self.register_log('Card included: ' + self.get_parsed_card('name') + ' - ' + set_code)
                    count += 1
                else:
                    self.register_log('Data : ' + str(self.card_data))
                    self.register_log('ERRO cart: ' + self.get_parsed_card('name'))
                    return
            self.main_em.connection.commit()
        self.main_em.cursor.close()

        self.register_log('Total cards: ' + str(count))
        self.register_log('Including Cards - End')

    def process_meta_card_info(self):
        self.register_log('Including Extra Info Cards - Start')
        count = 0
        self.main_em.create_connection_cursor()
        for key in self.json_data:
            self.set_mtg = self.json_data[key]
            set_code = self.get_parsed_mtg('code')
            self.cards = self.json_data[key]['cards']
            for card_data in self.cards:
                self.card_data = card_data
                self.current_card_id = self.get_card_id()
                mci_number = None
                magic_cards_info_code = None
                if (self.card_data.has_key('mciNumber') and self.card_data['mciNumber']):
                    mci_number = self.card_data['mciNumber']
                    if '/' in self.card_data['mciNumber']:
                        mci_number = self.card_data['mciNumber'].split('/')[-1]

                if (self.set_mtg.has_key('magicCardsInfoCode') and self.set_mtg['magicCardsInfoCode']):
                    magic_cards_info_code = self.set_mtg['magicCardsInfoCode']
                try:
                    self.process_images(magic_cards_info_code, mci_number, self.card_data['id'])
                    self.process_magic_card_color()
                    self.register_log('Card color included: ' + self.get_parsed_card('name'))
                    self.process_magic_card_color_identity()
                    self.register_log('Card color identity included: ' + self.get_parsed_card('name'))
                    self.process_magic_card_foreign_name()
                    self.register_log('Card foreign name: ' + self.get_parsed_card('name'))
                    self.process_magic_card_name()
                    self.register_log('Card name (dual): ' + self.get_parsed_card('name'))
                    self.process_magic_card_printing()
                    self.register_log('Card printing: ' + self.get_parsed_card('name'))
                    self.process_magic_card_ruling()
                    self.register_log('Card ruling: ' + self.get_parsed_card('name'))
                    self.process_magic_sub_type()
                    self.register_log('Card subtype: ' + self.get_parsed_card('name'))
                    self.process_magic_card_super_type()
                    self.register_log('Card supertype: ' + self.get_parsed_card('name'))
                    self.process_magic_magic_card_type()
                    self.register_log('Card type: ' + self.get_parsed_card('name'))
                    self.process_magic_card_variation()
                    self.register_log('Card variation: ' + self.get_parsed_card('name'))
                    self.process_magic_card_legality()
                    self.register_log('Card legality: ' + self.get_parsed_card('name'))
                    self.register_log('Card extra info processed: ' + self.get_parsed_card('name'))
                except:
                    import pdb;pdb.set_trace()
                    self.register_log('Card extra info error: ' + self.get_parsed_card('name') + ' - ' + str(self.current_card_id))
                    return
            self.main_em.connection.commit()
        self.main_em.cursor.close()
            
    def insert_card_sql_query(self, query, query_params=None):
        try:
            self.main_em.insert_batch(query, query_params)
        except:
            self.register_log('Card extra info error SQL: ' + query)

    def process_sub_card_info(self, base_query, base_list):
        for item in base_list:
            query_params = (self.get_parsed_string(str(item)), self.current_card_id)
            self.insert_card_sql_query(base_query, query_params)

    def process_magic_card_color(self):
        if self.card_data.has_key('colors'):
            base_query = """ INSERT INTO poyta.magic_card_color (card_color, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['colors'])

    def process_magic_card_color_identity(self):
        if self.card_data.has_key('colorIdentity'):
            base_query = """ INSERT INTO poyta.magic_card_color_identity (card_color_identity, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['colorIdentity'])


    def get_magic_card_language_label(self):
        result = ['English',]
        if self.card_data.has_key('foreignNames'):
            for item in self.card_data['foreignNames']:
                result.append(item['language'])
        return result

    def process_magic_card_foreign_name(self):
        if self.card_data.has_key('foreignNames'):
            base_query = """ INSERT INTO poyta.magic_card_foreign_name (foreign_language, foreign_name, id_magic_card) VALUES (%s, %s, %s) """
            self.card_data['foreignNames'].append({'language': 'English', 'name': self.card_data['name']})
            for item in self.card_data['foreignNames']:
                query_params = (self.get_parsed_string(item['language']), self.get_parsed_string(item['name']), self.current_card_id)
                self.insert_card_sql_query(base_query, query_params)

    def process_magic_card_name(self):
        if self.card_data.has_key('names'):
            base_query = """ INSERT INTO poyta.magic_card_name (card_name, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['names'])

    def process_magic_card_printing(self):
        if self.card_data.has_key('printings'):
            base_query = """ INSERT INTO poyta.magic_card_printing (id_magic_set, id_magic_card) VALUES ( (SELECT a.ID FROM poyta.magic_set a WHERE a.code = %s), %s) """
            self.process_sub_card_info(base_query, self.card_data['printings'])
       
    def process_magic_card_ruling(self):
        if self.card_data.has_key('rulings'):
            base_query = """ INSERT INTO poyta.magic_card_ruling (ruling_date, ruling_text, id_magic_card) VALUES (%s, %s, %s) """
            for item in self.card_data['rulings']:
                query_params = (self.get_parsed_string(item['date']), self.get_parsed_string(item['text']), self.current_card_id)
                self.insert_card_sql_query(base_query, query_params)

    def process_magic_sub_type(self):
        if self.card_data.has_key('subtypes'):
            base_query = """ INSERT INTO poyta.magic_card_sub_type (card_sub_type, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['subtypes'])
            
    def process_magic_card_super_type(self):
        if self.card_data.has_key('supertypes'):
            base_query = """ INSERT INTO poyta.magic_card_super_type (card_super_type, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['supertypes'])
            
    def process_magic_magic_card_type(self):
        if self.card_data.has_key('types'):
            base_query = """ INSERT INTO poyta.magic_card_type (card_type, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['types'])
            
    def process_magic_card_variation(self):
        if self.card_data.has_key('variations'):
            base_query = """ INSERT INTO poyta.magic_card_variation (card_variation, id_magic_card) VALUES ( %s, %s) """
            self.process_sub_card_info(base_query, self.card_data['variations'])

    def process_magic_card_legality(self):
        if self.card_data.has_key('legalities'):
            base_query = """ INSERT INTO poyta.magic_card_legality (format, status, id_magic_card) VALUES (%s, %s, %s) """
            for item in self.card_data['legalities']:
                query_params = (self.get_parsed_string(item['format']), self.get_parsed_string(item['legality']), self.current_card_id)
                self.insert_card_sql_query(base_query, query_params)

    def get_image_ucode(self, img_code, language_code):
        base_code = self.set_mtg['code'] + self.get_parsed_card('name') + self.get_parsed_card('imageName')
        if img_code != None:
            base_code += img_code
        if language_code != None:
            base_code += language_code

        return hashlib.sha1(base_code).hexdigest()

    def process_json_file(self):
        try:
            path = os.path.join(self.config_bot['entry_file_default_path'], 'json', 'mtg_cards', 'SET_' + self.get_parsed_mtg('code'))
            self.check_set_dir(path)
            printed_set = self.set_mtg.copy()
            printed_set['cards'] = []
            printed_set['cards'].append(self.card_data)
            path = os.path.join(path,  self.get_parsed_card('id') + '.json')
            if not os.path.isfile(path):
                with open(path, 'w') as fileJson:
                    fileJson.writelines(str(printed_set))
                    fileJson.close()
        except:
            print 'deu ruim'
            import pdb;pdb.set_trace()

    def process_images(self, set_code, img_code, card_ucode, download_img=False):
#        if img_code == None:
#            import pdb;pdb.set_trace()
        base_query = """ INSERT INTO poyta.magic_card_image (id_magic_card, mci_image_code, mci_url, image_ucode, languange_code) VALUES (%s, %s, %s, %s, %s) """
        
        for item in self.get_magic_card_language_label():
            
            language_code = LANGUAGE_DICT[item]
    
            url = None  
            if img_code != None and set_code != None:
                url = BASE_IMAGE_URL.replace('<LANGUAGE_CODE>', language_code).replace('<set_code>', set_code).replace('<card_name>', img_code)

            self.current_card_id = self.get_card_id()

            query_params = (self.current_card_id, img_code,  url, self.get_image_ucode(img_code, language_code), language_code)
            self.insert_card_sql_query(base_query, query_params)

            if img_code != None and set_code != None:
                path = os.path.join(self.config_bot['mtg_cards_path'], 'SET' + set_code, language_code)
                self.check_set_dir(path)
                path = os.path.join(path, card_ucode + '.jpg')
                if download_img and img_code != None:
                    if not os.path.isfile(path):
                        f = urllib.urlopen(url)
                        with open(path, 'wb') as imgFile:
                            imgFile.write(f.read())
                            imgFile.close()
                        self.register_log('Card Image Downloaded included: ' + self.get_parsed_card('name') + ' - ' + url) 
                        #urllib.urlretrieve(url, path)

    def check_set_dir(self, directory):
        if not os.path.exists(directory):
            os.makedirs(directory)
    