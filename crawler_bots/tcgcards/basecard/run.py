import sys
sys.path.append('../..')

from robots import config
from BaseCardBot import BaseCardBot

if __name__ == "__main__":
    base_card_bot = BaseCardBot()
    base_card_bot.run_bot()
    