# -*- coding: utf-8 -*-

import config
import os
import time

class BaseObject:

    def __init__(self, bot_key):
        self.bot_key = bot_key
        self.file_log = None

    def start_log(self):
        path = os.path.join(config.log_default_path, self.bot_key + '.log' )
        self.file_log = open(path , 'a')
        self.register_log('Process Start')

    def close_log(self):
        self.file_log.close()

    def print_msg(self, msg):
        msg = ' [' + time.strftime("%c") + '] - ' + msg
        print msg
        
    def register_log(self, msg):
        msg = ' [' + time.strftime("%c") + '] - ' + msg
        self.file_log.write(msg + '\n')
        print msg
