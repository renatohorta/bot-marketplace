# -*- coding: utf-8 -*-

import MySQLdb as mdb
import config
import os
import time

from EntityManager import EntityManager
from BaseObject import BaseObject

QUERY_BOT_LUPA = """ SELECT c.parameter_name, c.parameter_value, b.bot_key
                     FROM bot_marketplace.bot b inner join bot_marketplace.config c on b.id = c.id_bot 
                     WHERE b.is_active = 1 and b.bot_key = '<BOT_KEY>' """

class BaseBot(BaseObject):


    # 1 - Carregar configuração de bot
    # 2 - Criar conector de bot
    # 3 - iniciar log
    
    def __init__(self, bot_key):
        BaseObject.__init__(self, bot_key)
        self.config_base = {}
        self.config_bot = {}
        self.main_em = None
        self.config_em = None
        self.file_log = None
        self.is_active = True

    def load_config_dict(self):
        self.config_base = {}
        self.config_base['connection_server'] = config.config_db_server
        self.config_base['connection_login'] = config.config_db_login
        self.config_base['connection_password'] = config.config_db_password
        self.config_base['connection_database'] = config.config_db_name

    def load_config_em(self):
        self.load_config_dict()
        self.config_em = EntityManager(self.config_base, self.bot_key)
        self.register_log('Config EntityManager loaded')
        cursor = self.config_em.cursor
        query = QUERY_BOT_LUPA.replace('<BOT_KEY>', self.bot_key)
        cursor.execute(query)
        result = cursor.fetchall()
        if not result or len(result) == 0:
            self.is_active = False
            self.register_log('Bot %s is inactive' % self.bot_key)
            return
        self.is_active = True
        self.config_bot = {}
        for item in result:
            self.config_bot[item[0]] = item[1]
        self.register_log('Config Bot loaded')
        
    def load_main_em(self):
        if self.is_active:
            self.main_em = EntityManager(self.config_bot, self.bot_key)
            self.register_log('Main EntityManager loaded')

    def run_bot(self):
        self.start_log()
        self.load_config_em()
        self.load_main_em()
        if self.is_active:
            self.register_log('Bot Processing - Start')
            self.execute()
            self.register_log('Bot Processing - End')
        else:
            self.register_log('Bot Not Processed ')
        self.close_log()
        pass
        
    def execute(self):
        pass
    